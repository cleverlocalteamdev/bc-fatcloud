$(function() {
  	// Bigcommerce Country Selector Bug Fix
	
	countrySelectorFix();
});

// Functions

function countrySelectorFix() {
	// Checkout Page Checker
	if($('.ExpressCheckout').length != 0) {
		$(document).ajaxComplete(function() {
			$('#uniform-FormField_11').change(function() {
				$(this).find('option[value=""]').remove();
			});
		});
	}
}
